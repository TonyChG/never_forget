#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name="never_forget",
    version="0.0.1",
    author="TonyChG",
    author_email="tonychg7@pm.me",
    long_description="",
    url="git@gitlab.com:tonychg/never_forget.git",
    packages=find_packages(),
    entry_points={
        "console_scripts": ["nf = never_forget.__main__:main"],
    },
    install_requires=[
        "rich==10.3.0",
        "click==8.0.1",
        "youtube-dl==2021.6.6",
        "minio==7.1.0",
    ],
    include_package_data=True,
    python_requires=">=3.6",
    classifiers=[
        "Development Status :: 5 - In developpment",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "License :: Apache License Version 2.0",
        "Programming Language :: Python :: 3",
    ],
)
