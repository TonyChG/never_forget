# coding: utf8
# vi: ft=python expandtab ts=4 sts=4 sw=4

import sys

from never_forget.commands import download


def main():
    try:
        download()
    except KeyboardInterrupt:
        sys.exit(0)
