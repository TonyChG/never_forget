# coding: utf8
# vi: ft=python expandtab ts=4 sts=4 sw=4

import logging

import youtube_dl


class _Logger:
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        logging.error(msg)


class YDLWrapper:
    def __init__(self, params: dict = {}, verbosity: bool = False):
        self._params = params
        self._finished = []
        self._urls = None
        self._verbosity = verbosity

    def _status_callback(self, download):
        if download["status"] == "finished":
            logging.info(
                'Done downloading, now converting "%s"', download["filename"]
            )
            self._finished.append(download)

    def download(self, urls: list[str], destination: str) -> dict[str, dict]:
        _defaults_params = {
            "format": "best",
            "outtmpl": "{}/%(title)s.%(ext)s".format(destination),
            "logger": _Logger(),
            "progress_hooks": [self._status_callback],
        }

        if self._verbosity:
            logging.info("Starting downloads in directory %s :", destination)
            for url in urls:
                logging.info(url)

        with youtube_dl.YoutubeDL({**_defaults_params, **self._params}) as ydl:
            self._urls = tuple(urls)
            ydl.download(urls)
        return dict(zip(self._urls, self._finished))


def youtube_download(urls: list[str], destination: str, verbosity: bool):
    ydl = YDLWrapper(verbosity=verbosity)

    try:
        downloads = ydl.download(urls, destination)
    except youtube_dl.utils.DownloadError:
        logging.error("Download failed check input urls")
    else:
        return downloads
