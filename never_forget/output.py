# coding: utf8
# vi: ft=python expandtab ts=4 sts=4 sw=4

import logging

from rich.logging import RichHandler


def setup_logging(debug: bool):
    logging.basicConfig(
        level="NOTSET" if debug else logging.INFO,
        format="%(message)s",
        datefmt="[%X]",
        handlers=[RichHandler(rich_tracebacks=debug)],
    )
