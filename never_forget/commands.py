# coding: utf8
# vi: ft=python expandtab ts=4 sts=4 sw=4

import os
import logging

import click

from never_forget.output import setup_logging
from never_forget.downloaders.youtube_dl import youtube_download
from never_forget.uploaders.minio import (
    create_client,
)


@click.command()
@click.option("-u", "--url", multiple=True, type=str)
@click.option("-d", "--destination", default=os.path.abspath(os.path.curdir))
@click.option("--debug", is_flag=True)
@click.option("-V", "--verbosity", is_flag=True)
@click.option("--minio-config", default="~/.mc/config.json", type=str)
@click.option("--minio", type=str, help="Minio configuration <alias>/<bucket>")
@click.option(
    "--rm", is_flag=True, help="Remove local file after successfully upload"
)
def download(
    url: list[str],
    destination: str,
    debug: bool,
    verbosity: bool,
    minio_config: str,
    minio: str,
    rm: bool,
):
    setup_logging(debug)

    downloads = youtube_download(url, destination, verbosity)

    for url, download in downloads.items():
        logging.info('%s available at "%s"', url, download["filename"])

    if minio:
        minio_server, minio_bucket = minio.split("/")
        client = create_client(minio_config, minio_server)

        for download in downloads.values():
            filepath = download["filename"]
            filename = os.path.basename(download["filename"])

            logging.info(
                "Upload %s to %s/%s", filename, minio_server, minio_bucket
            )
            client.fput_object(minio_bucket, filename, filepath)

            if rm:
                os.unlink(filepath)
