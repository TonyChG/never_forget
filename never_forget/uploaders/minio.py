# coding: utf8
# vi: ft=python expandtab ts=4 sts=4 sw=4

import os
import json
import logging
from urllib.parse import urlparse

from minio import Minio


def get_credentials(path, minio_server):
    try:
        with open(os.path.expanduser(path)) as fd:
            conf = json.load(fd)
            aliases = conf.get("aliases", {})
            return aliases.get(minio_server)
    except OSError as error:
        logging.error(error)


def create_client(minio, minio_server):
    credentials = get_credentials(minio, minio_server)

    if credentials is not None:
        endpoint = urlparse(credentials.get("url"))

        return Minio(
            endpoint.netloc,
            credentials.get("accessKey"),
            credentials.get("secretKey"),
            secure=True if endpoint.scheme == "https" else False,
        )
