# never_forget CLI

## TLDR

Utilisation de Python pour faciliter la sauvegarde de medias téléchargés en ligne.

Les vidéos supprimées par les auteurs ne seront plus jamais indisponibles.

## Installation

```
python3.9 setup install
```

## Usage

Toutes les URLs supportées par `youtube-dl` sont disponibles au téléchargement.

On peut trouver une liste des extractors sur ce
[lien](https://github.com/ytdl-org/youtube-dl/tree/master/youtube_dl/extractor).

```
nf -u https://www.youtube.com/watch?v=xvkh9OJtAA8 -d /tmp --minio s3/never_forget
nf -u https://www.twitch.tv/sardoche/clip/BadAdorableGoblinFloof-FMBcQLZH6BGL_oSw --minio s3/never_forget
```

Pour que l'upload sur [Minio](https://docs.min.io/docs/minio-client-quickstart-guide.html)
fonctionne, il faut que le fichier de configuration
par défaut soit présent. Vous pouvez ajouter une configuration en exécutant la
commande suivante.

```
set +o history
mc alias set myminio http://localhost:9000 minio minio123 --api "s3v4" --path "off"
set -o history
```


Il aussi possible de fournir un fichier de configuration directement via
l'option `--minio-config`

```
nf -u https://www.youtube.com/watch?v=xvkh9OJtAA8 --minio s3/never_forget --minio-config ./config.json
```

## TODOs

- support d'autre système de sauvegarde
	- [ ] Dropbox
	- [ ] Google Drive
	- [ ] Telegram
	- [ ] Owncloud
- support d'autre système de téléchargement
